<?php
return [

    // FORMAT EXAMPLE.
    //
    // 'extension_tracker' => [
    //     'key' => 'extension_tracker',
    //     'wiki_key' => 'Extension_Tracker',
    //     'creators' => json_encode([
    //         'Uri' => 'https://github.com/preimpression/',
    //     ]),
    //     'version' => '1.0.0',
    // ],
    
    'mini_clock' => [
        'key' => 'mini_clock',
        'wiki_key' => 'Mini_Clock',
        'creators' => json_encode([
            'Newt' => 'https://github.com/ne-wt/',
        ]),
        'version' => '1.0.0',
    ],


    'theme_manager' => [
        'key' => 'theme_manager',
        'wiki_key' => 'Theme Manager',
        'creators' => json_encode([
            'Uri' => 'https://github.com/preimpression/',
        ]),
        'version' => '1.0.0',
    ],
    'multiple_character_creation' => [
        'key' => 'Multiple_Character_Creation',
        'wiki_key' => 'Multiple_Character_Creation',
        'creators' => json_encode([
            'Mercury' => 'https://github.com/itinerare/',
        ]),
        'version' => '1.0.0',
    ],
    'choice_box_tag' => [
        'key' => 'Choice_Box_Tag',
        'wiki_key' => 'Choice_Box_Tag',
        'creators' => json_encode([
            'Mercury' => 'https://github.com/itinerare/',
        ]),
        'version' => '1.0.0',
    ],
    'extension_tracker' => [
        'key' => 'shop_features',
        'wiki_key' => 'Shop_Features',
        'creators' => json_encode([
            'Newt' => 'https://github.com/ne-wt/',
        ]),
        'version' => '1.0.3',
    ],
    'awards' => [
        'key' => 'awards',
        'wiki_key' => 'Awards',
        'creators' => json_encode([
            'Uri' => 'https://github.com/preimpression/',
            'TGI' => 'https://github.com/tjgallaha/',
        ]),
        'version' => '1.3.0',
    ]

];
